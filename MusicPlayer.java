import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MusicPlayer {

	private MediaPlayer mediaPlayer;
	private ArrayList<Media> playlist;
	
	
	public MusicPlayer(){
		playlist = new ArrayList<Media>();
        createPlaylist();
        
        String bip = "C:/Users/Florian/Music/Music/Eric Prydz - Call On Me.mp3";
        Media hit = new Media(new File(bip).toURI().toString());
        mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setVolume(0.04);
	}
	
	public void startMusic(){
		mediaPlayer.play();
	}
	

    public void shuffle(){
    	 mediaPlayer.stop();
    	 Random rd = new Random();
    	 int x = rd.nextInt(playlist.size());
    	 mediaPlayer = new MediaPlayer(playlist.get(x));
         mediaPlayer.play();
    	
    }
	
    public void addSong(String songPath){
    	playlist.add(new Media(new File(songPath).toURI().toString()));
    }
    
    private void createPlaylist(){
    	addSong("C:/Users/Florian/Music/Music/Genesis - Invisible Touch.mp3");
    	addSong("C:/Users/Florian/Music/Music/Pussy.mp3");
    	addSong("C:/Users/Florian/Music/Music/Sido - Kebab.mp3");
    	addSong("C:/Users/Florian/Music/Music/00 Schneider.mp3");
    	addSong("C:/Users/Florian/Music/Music/Eagle Eye Cherry - Save tonight.mp3");
    	addSong("C:/Users/Florian/Music/Music/Big Shaq - The Ting Goes Da Ba Dee Da Ba Die.mp3");
    	addSong("C:/Users/Florian/Music/Music/M83 - Midnight City.mp3");
    	addSong("C:/Users/Florian/Music/Music/Demons - Imagine Dragons.mp3");
    }
    
}
