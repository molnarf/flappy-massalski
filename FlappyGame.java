import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class FlappyGame {

	public Bird baltasar;
	public ArrayList<Pipe> pipes;
	
	public boolean ingame;
	public int vertical_dist;	// dist between top and bottom pipe
	private static final String pipePath = "C:/Users/Florian/Pictures/Saved Pictures/Pipe.png";
	private static final String pipePathDown = "C:/Users/Florian/Pictures/Saved Pictures/Pipe_downside.png";
	private int window_width;
	private int window_height;
	private int score;
	private int distToNextPipes = 300;
	private MediaPlayer flapSound;
	
	public FlappyGame(int window_width, int window_height){
//		this.baltasar = new Bird(100, 150, "C:/Users/Florian/Pictures/Saved Pictures/Louis_XIV_of_France.jpg");
		this.baltasar = new Bird(100, 150, "C:/Users/Florian/Pictures/Saved Pictures/Ovall.png");
		this.pipes = new ArrayList<>();
		this.flapSound = new MediaPlayer(new Media(new File("C:/Users/Florian/Documents/Testdatein/sounds/Flap.mp3").toURI().toString()));
		
		this.ingame = true;
		this.window_height = window_height;
		this.window_width = window_width;
		
		this.vertical_dist = Pipe.height + 160; 

		addNewPipes();
	}
	
	
	public void update(){
		
		if(pipes.get(0).canBeDeleted()){
			pipes.remove(0);
			pipes.remove(0);
		}
		
		for(Pipe pipe : pipes){
			pipe.update();
		}
		baltasar.update();
		
		
		addNewPipesAfterDistance(distToNextPipes, 600);

		checkCollisions();
		updateScore();
	}
	
	public void checkCollisions(){
		
		for(Pipe pipe : pipes){
			if(baltasar.intersects(pipe.getBoundary())){
				ingame = false;
			}
		}
		
		if(baltasar.touchesGround(window_height) || baltasar.flewTooHigh()){
			ingame = false;
		}
	}
	
	public void updateScore(){
		for(Pipe pipe : pipes){
			if(baltasar.getPosX() == pipe.getPosX() + Pipe.width){
				score++;
				return;
			}
		}
	}
	
	
	/**
	 * adds a new pipe that has a certain distance to the latest one
	 * @param distance the two pipe pairs should have 
	 * @param width of the window
	 */
	public void addNewPipesAfterDistance(int distance, int width){
		double lastPipePosX = pipes.get(pipes.size()-1).getPosX();
		
		
		if(width - lastPipePosX + Pipe.width > distance){
			addNewPipes();
		}
	}
	
	public void addNewPipes(){
		Random rd = new Random();
		int yPos = - rd.nextInt(Pipe.height - 80) - 65;	// - 80 and - 65 because i dont want it to be exactely either end of the screen

		pipes.add(new Pipe(window_width + Pipe.width, yPos, pipePathDown, true));		// doesnt matter if true or false when its
		pipes.add(new Pipe(window_width + Pipe.width, yPos + vertical_dist, pipePath, false));	// not an AI game
	}
	
	
	public int getScore(){
		return score;
	}
	
	public void keyPressed(KeyEvent event){
	   	KeyCode key = event.getCode();

	   	if(key == KeyCode.SPACE){
	   		baltasar.jump();
	   		
	   		flapSound = new MediaPlayer(new Media(new File("C:/Users/Florian/Documents/Testdatein/sounds/Flap.mp3").toURI().toString()));
	        flapSound.play();
	   	}
	}
	 
	 
}
