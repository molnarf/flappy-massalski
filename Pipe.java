import java.io.File;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class Pipe {

	private Image image;
	private double velocityX;
	private double xPos;
	private double yPos;
	public static final int width = 100;
	public static final int height = 300;
	
	public boolean isTopPipe;
	
	
	public Pipe(int xPos, int yPos, String imagePath, boolean isTopPipe){
		this.xPos = xPos;
		this.yPos = yPos;
		this.velocityX = -5;
		this.image = new Image(new File(imagePath).toURI().toString(), width, height, false, false);
		this.isTopPipe = isTopPipe;
	}
	
	public boolean canBeDeleted(){
		if(this.xPos < -width){
			return true;
		}
		return false;
	}
	
	public void update(){
		this.xPos += velocityX;
	}
	
	public void render(GraphicsContext gc){
        gc.drawImage(image, xPos, yPos);
    }
	

	public Shape getBoundary(){
		Shape shape = new Rectangle(xPos, yPos, width, height);
		return shape;
    }
 
   public boolean intersects(Shape s){
    	Shape intersection = Shape.intersect(s, this.getBoundary());
    	if (((Path) intersection).getElements().size() > 0) {		// checks if the intersection is empty between both shapes
            return true;
        }
    	return false;
    }


	public double getPosX(){
		return this.xPos;
	}
	
	public double getPosY(){
		return this.yPos;
	}
	
}
