import java.util.ArrayList;
import java.util.Random;

import javafx.scene.canvas.GraphicsContext;

public class Population {

	public ArrayList<Player> players;
	
	public int numPlayers;
	public int numGen = 1;
	public int numPlayersAlive;
	
	private Player currentBestPlayer;
//	public double learnRate = 0.1;
	
	
	public Population(int numPlayers){
		this.players = new ArrayList<>();
		this.numPlayers = numPlayers;
		
		addRandomPlayer(numPlayers);
	}
	
	
	public void update(){
		numPlayersAlive = 0;
		for(Player player : players){
			player.update();
			if(player.isAlive){
				numPlayersAlive++;
			}
		}
	}
	
	public boolean AllDead(){
		for(Player player : players){
			if(player.isAlive){
				return false;
			}
		}
		return true;
	}
	
	
	public void render(GraphicsContext gc){
		for(Player player : players){
			player.render(gc);
		}
	}
	
	
	public void determineBestPlayer(){
		Player bestPlayer = players.get(0);
		for(Player player : players){
			if(player.survivalTime > bestPlayer.survivalTime){
				bestPlayer = player;
			}
		}
		
		System.out.println(bestPlayer.score);

		bestPlayer.revive();
		bestPlayer.setImage("C:/Users/Florian/Pictures/Saved Pictures/Messias.png");
		
		bestPlayer.score = 0;
		bestPlayer.survivalTime = 0;

		this.currentBestPlayer = bestPlayer;
	}
	
	public void createOffspring(){
		determineBestPlayer();
		
		this.numGen++;
		
		this.players.clear();
		addRandomPlayer(100);
		
		for(int i = 0; i < numPlayers; i++){
			double[] w1_Jump = mutateArray(currentBestPlayer.w1_Jump);
			double[] w1_NoJump = mutateArray(currentBestPlayer.w1_NoJump);
			double[] b1 = mutateArray(currentBestPlayer.b1);
			
			Player newPlayer = new Player(100, 150, "C:/Users/Florian/Pictures/Saved Pictures/Ovall.png", w1_Jump, w1_NoJump, b1);
			
			players.add(newPlayer);
		}
		
		players.add(currentBestPlayer);
	}
	
//	---------------------------------------------------------------------------------------------------------------

	public void addRandomPlayer(int numRdPlayers){
		Random rd = new Random();
		for(int i = 0; i < numRdPlayers; i++){
			// initialized randomly
			double[] w1_Jump = new double[]{rd.nextInt(20)-10, rd.nextInt(20)-10, rd.nextInt(20)-10, rd.nextInt(20)-10};	// biases initialized with 0
			double[] w1_NoJump = new double[]{rd.nextInt(20)-10, rd.nextInt(20)-10, rd.nextInt(20)-10, rd.nextInt(20)-10};	// biases initialized with 0
			
			
//			bias of Jump and NoJump
			double[] b1 = new double[]{rd.nextInt(10)-5, rd.nextInt(10)-5};
			
			Player rdPlayer = new Player(100, 150, "C:/Users/Florian/Pictures/Saved Pictures/Ovall.png", w1_Jump, w1_NoJump, b1);
			
			players.add(rdPlayer);
		}
	}
	
	
	public double[] mutateArray(double[] array){
		Random rd = new Random();
		double[] mutatedArray = new double[array.length];
		
		for(int i = 0; i < array.length; i++){
			if(rd.nextBoolean()){
				mutatedArray[i] = array[i] + ((rd.nextDouble() - 0.5) * 1.1);
			}
			else{
				mutatedArray[i] = array[i] - ((rd.nextDouble() - 0.5) * 1.1);
			}
		}
		
		return mutatedArray;
	}
	
	
	
	

	
	
}
