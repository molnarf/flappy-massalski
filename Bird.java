import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.transform.Rotate;
import javafx.scene.shape.*;

public class Bird{

	private Image image;
	private double velocityY;
	private double rotationAngle;
	public double xPos;
	public double yPos;
	public static double birdWidth = 40;
	public static double birdHeight = 48;
	
	
	
	public Bird(int xPos, int yPos, String imagePath){
		this.xPos = xPos;
		this.yPos = yPos;
		this.rotationAngle = 90;
//		this.image = new Image(new File(imagePath).toURI().toString(), birdWidth, birdHeight, false, false);

		this.image = new Image(new File(imagePath).toURI().toString(), birdWidth, birdHeight, true, false);
	}
	
	public void jump(){
		velocityY = -12;
		rotationAngle = 90;
	}
	
	public void applyGravity(){
		if(velocityY < 9){
			velocityY += 0.8;
		}
	}
	
	public void update(){
		applyGravity();
		this.yPos += velocityY;
		
		if(rotationAngle < 160){
			rotationAngle += velocityY * 0.8;
		}
	}
	
	public void render(GraphicsContext gc){
      drawRotatedImage(gc, image, rotationAngle, xPos, yPos);
    }
	
	 /**
     * Sets the transform for the GraphicsContext to rotate around a pivot point.
     *
     * @param gc the graphics context the transform to applied to.
     * @param angle the angle of rotation.
     * @param px the x pivot co-ordinate for the rotation (in canvas co-ordinates).
     * @param py the y pivot co-ordinate for the rotation (in canvas co-ordinates).
     */
    private void rotate(GraphicsContext gc, double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }
    
    /**
     * Draws an image on a graphics context.
     *
     * The image is drawn at (tlpx, tlpy) rotated by angle pivoted around the point:
     *   (tlpx + image.getWidth() / 2, tlpy + image.getHeight() / 2)
     *
     * @param gc the graphics context the image is to be drawn on.
     * @param angle the angle of rotation.
     * @param tlpx the top left x co-ordinate where the image will be plotted (in canvas co-ordinates).
     * @param tlpy the top left y co-ordinate where the image will be plotted (in canvas co-ordinates).
     */
    private void drawRotatedImage(GraphicsContext gc, Image image, double angle, double tlpx, double tlpy) {
        gc.save(); // saves the current state on stack, including the current transform
        rotate(gc, angle, tlpx + image.getWidth() / 2, tlpy + image.getHeight() / 2);
        gc.drawImage(image, tlpx, tlpy);
        gc.restore(); // back to original state (before rotation)
    }
	
	public Shape getBoundary(){
		Shape ellipse = new Ellipse(xPos + birdWidth/2,
				yPos + birdHeight/2, birdWidth/2, birdHeight/2);
		
		ellipse.setRotate(rotationAngle);
		return ellipse;
    }

    
    public boolean intersects(Shape s){
    	Shape intersection = Shape.intersect(s, this.getBoundary());
    	if (((Path) intersection).getElements().size() > 0) {		// checks if the intersection is empty between both shapes
            return true;
        }
    	return false;
    }
	
    public boolean touchesGround(int groundPos){
    	if(this.yPos + birdHeight > groundPos){
    		return true;
    	}
    	return false;
    }
    
    public boolean flewTooHigh(){
    	return this.yPos < -50;
    }
    
    
    public double getPosX(){
    	return this.xPos;
    }
    
    public double getPosY(){
    	return this.yPos;
    }
    
    public void setImage(String imagePath){
		this.image = new Image(new File(imagePath).toURI().toString(), birdWidth, birdHeight, true, false);
    }
}
