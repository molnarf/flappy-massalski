import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class FlappyView extends Application{

	private GraphicsContext gc;
	private Group root;
	private MyAnimationTimer timer;
	public static final int width = 600;
	public static final int height = 400;
	
	private FlappyGame game;
	
	public AI_FlappyGame ai_game;
	private Population population;
	
	private MyBackground background;
	
	public boolean isAI_GAME = true;
	

	
	public void createWindow(String[] args){
		launch(args);
	}

	
	@Override
    public void start(Stage primaryStage) throws Exception{
		this.root = new Group();
		Scene scene = new Scene(root);
		scene.setOnKeyPressed(new TAdapter());
        Canvas canvas = new Canvas(width, height);
		root.getChildren().add(canvas);
		this.gc = canvas.getGraphicsContext2D();

		
		this.timer = new MyAnimationTimer(this);
		
		timer.start();
		
		// ai game is different from human player game
		if(isAI_GAME){
			this.population = new Population(300);
			ai_game = new AI_FlappyGame(width, height, population);
		}
		else{
			this.game = new FlappyGame(width, height);
		}
		
		this.background = new MyBackground("C:/Users/Florian/Pictures/Saved Pictures/sky.jpg");
		
		
		addMusicButton();
		primaryStage.setScene(scene);
	    primaryStage.setTitle("Flappy Massalski");
	    primaryStage.show();
	}
	
	
	public void paintComponents(){
		if(isAI_GAME){
			paintComponents_AI();
			return;
		}
		
		gc.setFill(Color.BLACK);
		gc.setFont(new Font("aerial", 40));
		
		background.update();	// the background gets updated seperately because its part of the view not the game
		background.render(gc);

		for(Pipe pipe : game.pipes){
			pipe.render(gc);
		}
		game.baltasar.render(gc);
		
		
		gc.fillText("" +game.getScore(), width/2 - 20, 30);
	}
	
	public void paintComponents_AI(){
		gc.setFill(Color.BLACK);
		gc.setFont(new Font("aerial", 40));
		
		background.update();	// the background gets updated seperately because its part of the view not the game
		background.render(gc);

		for(Pipe pipe : ai_game.pipes){
			pipe.render(gc);
		}
		
		ai_game.population.render(gc);
		
		gc.fillText("" + ai_game.getScore(), width/2 - 20, 30);
		
		gc.setFont(new Font("aerial", 20));
		gc.fillText("Gen: " + population.numGen, 0, 30);
		gc.fillText("Still alive: " + population.numPlayersAlive, width - 140, height - 30);
	}
	
	
	public void addMusicButton(){
		VBox vBox = new VBox();
		vBox.setMinSize(100, 100);
		vBox.setLayoutX(width-100);
		vBox.setAlignment(Pos.TOP_RIGHT);

//		String style = "-fx-background-color: rgba(122, 255, 255, 0.5);";
//		vBox.setStyle(style);
		
		MusicPlayer mp = new MusicPlayer();
		mp.startMusic();
		
		Button button = new Button("Shuffle");
		
		
		vBox.getChildren().add(button);
		button.setFocusTraversable(false);
		
		button.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	mp.shuffle();
		    }
		});
		
		this.root.getChildren().add(vBox);
	}
	
	public void resetButton(){
		VBox vBox = new VBox();
		vBox.setMinSize(width, 50);
		vBox.setLayoutY(height/2 - 50);
		vBox.setAlignment(Pos.CENTER);
		
		Button button = new Button("Reset");
		vBox.getChildren().add(button);
		
		button.setOnAction(event ->{
        	this.game = new FlappyGame(width, height);
        	this.root.getChildren().remove(vBox);
        	this.timer.start();
        });
		this.root.getChildren().add(vBox);
	}
	
	public void resetButton_AI(){
		VBox vBox = new VBox();
		vBox.setMinSize(width, 50);
		vBox.setLayoutY(height/2 - 50);
		vBox.setAlignment(Pos.CENTER);
		
		Button button = new Button("Reset");
		vBox.getChildren().add(button);
		
		button.setOnAction(event ->{
			population.createOffspring();
        	this.ai_game = new AI_FlappyGame(width, height, population);
        	this.root.getChildren().remove(vBox);
        	this.timer.start();
        });
		this.root.getChildren().add(vBox);
	}
	
	
    private class TAdapter implements javafx.event.EventHandler<KeyEvent>{

		@Override
		public void handle(KeyEvent event) {
			// TODO Auto-generated method stub
			if(event.getEventType() == KeyEvent.KEY_PRESSED){
				if(!isAI_GAME){
					game.keyPressed(event);
				}
			}
		}
		
	}
	
	public FlappyGame getGame(){
		return this.game;
	}
	
}
