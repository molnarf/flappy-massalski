import java.util.ArrayList;
import java.util.Random;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class AI_FlappyGame {

	public ArrayList<Pipe> pipes;
	
	public boolean ingame;
	public int vertical_dist; // vertical dist to bottom pipe
	private static final String pipePath = "C:/Users/Florian/Pictures/Saved Pictures/Pipe.png";
	private static final String pipePathDown = "C:/Users/Florian/Pictures/Saved Pictures/Pipe_downside.png";
	private int window_width;
	private int window_height;
	private int score;
	private int distToNextPipes = 300;
	
	public Population population;
	
	public AI_FlappyGame(int window_width, int window_height, Population population){
		this.population = population;
		
		
		this.pipes = new ArrayList<>();
		
		this.ingame = true;
		this.window_height = window_height;
		this.window_width = window_width;
		
		this.vertical_dist = Pipe.height + 160; 

		addNewPipes();
	}
	
	
	public void update(){
		
		if(pipes.get(0).canBeDeleted()){
			pipes.remove(0);
			pipes.remove(0);
		}
		
		for(Pipe pipe : pipes){
			pipe.update();
		}
		
		for(Player player : population.players){
			if(player.isAlive){
				player.distToBottomPipe = yPosBottomPipe() - player.getPosY();
				player.distToTopPipe = yPosTopPipe() - player.getPosY();
				player.distToPipe = distToNextPipePos();
				player.score = this.score;
			}
		}
		
		population.update();
		
		
		addNewPipesAfterDistance(distToNextPipes, 600);

		checkCollisions();
		updateScore();
	}
	
	public void checkCollisions(){
		
		for(Pipe pipe : pipes){
			for(Player player : population.players){
				player.intersects(pipe.getBoundary());
			}
		}
		
		for(Player player : population.players){
			player.touchesGround(window_height);
			player.flewTooHigh();
		}
		
		if(population.AllDead()){
			ingame = false;
		}
		
	}
	
	public void updateScore(){
		for(Pipe pipe : pipes){
			if(100 == pipe.getPosX() + Pipe.width){
				score++;
				return;
			}
		}
	}
	
	
	/**
	 * adds a new pipe that has a certain distance to the latest one
	 * @param distance the two pipe pairs should have 
	 * @param width of the window
	 */
	public void addNewPipesAfterDistance(int distance, int width){
		double lastPipePosX = pipes.get(pipes.size()-1).getPosX();
		//				10		 20
		//				.   .    |  
		if(width - lastPipePosX + Pipe.width > distance){
			addNewPipes();
		}
	}
	
	public void addNewPipes(){
		Random rd = new Random();
		int yPos = - rd.nextInt(Pipe.height - 80) - 65;	// - 80 and - 65 because i dont want it to be exactely either end of the screen

		pipes.add(new Pipe(window_width + Pipe.width, yPos, pipePathDown, true));
		pipes.add(new Pipe(window_width + Pipe.width, yPos + vertical_dist, pipePath, false));
	}
	
	
	public int getScore(){
		return score;
	}
	
	public double distToNextPipePos(){
		for(Pipe pipe : pipes){
			if(pipe.getPosX() >= 100){
				return pipe.getPosX() - 100;
			}
		}
		return 0;
	}
	
	public double yPosTopPipe(){
		for(Pipe pipe : pipes){
			if(pipe.getPosX() >= 100 - Pipe.width && pipe.isTopPipe){
				return pipe.getPosY() + Pipe.height;
			}
		}
		throw new RuntimeException("Something went wrong");
	}
	
	
	public double yPosBottomPipe(){
		for(Pipe pipe : pipes){
			if(pipe.getPosX() >= 100 - Pipe.width && !pipe.isTopPipe){
				return pipe.getPosY();
			}
		}
		throw new RuntimeException("Something went wrong");
	}
}
