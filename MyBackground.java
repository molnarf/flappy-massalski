import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class MyBackground {

	private Image image;
	private double velocityX = -1;
	private double xPos;
	public int width = FlappyView.width;
	public int height = FlappyView.height;
	
	
	public MyBackground(String imagePath){
		
		this.image = new Image(new File(imagePath).toURI().toString(), width, height, false, false);

	}
	
	public void update(){
		this.xPos += velocityX;
		
		if(xPos == -width){
			xPos = 0;
		}
	}
	
	public void render(GraphicsContext gc){
        gc.drawImage(image, xPos, 0);
        gc.drawImage(image, xPos + width, 0);
        
        
    }
}
