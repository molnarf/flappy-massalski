import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Shape;

public class Player extends Bird{

	public double distToPipe;
	public double distToTopPipe;
	public double distToBottomPipe;
	private double velocityY;
	public int score;
	public long survivalTime;
	
	public boolean isAlive;

	public double[] w1_Jump;
	public double[] w1_NoJump;
	public double[] b1;
	

	public Player(int xPos, int yPos, String imagePath, double[] w1_Jump, double[] w1_NoJump, double[] b1) {
		super(xPos, yPos, imagePath);
		
		this.w1_Jump = w1_Jump;
		this.w1_NoJump = w1_NoJump;
		this.b1 = b1;
		this.isAlive = true;
	}
	
	
	public void update(){
		if(isAlive){
			super.update();
			this.survivalTime = System.currentTimeMillis();
			jumps(distToPipe, distToTopPipe, distToBottomPipe, velocityY);
		}
	}
	
	public boolean intersects(Shape s){
    	boolean intersects = super.intersects(s);
    	if(intersects){
    		isAlive = false;
    	}
    	return intersects;
    }
	
    public boolean touchesGround(int groundPos){
    	boolean touchesGround = super.touchesGround(groundPos);
    	if(touchesGround){
    		isAlive = false;
    	}
    	return touchesGround;
    }
    
    public boolean flewTooHigh(){
    	boolean flewTooHigh = super.flewTooHigh();
    	if(flewTooHigh){
    		isAlive = false;
    	}
    	return flewTooHigh;
    }
    
    
    public void render(GraphicsContext gc){
    	if(!isAlive){
    		xPos -= 5;
    	}
    	if(xPos > -100){
    		super.render(gc);
    	}
    }
	
    public void revive(){
    	this.isAlive = true;
    	this.xPos = 100;
    	this.yPos = 150;
    }

	/**
	 * i wanna make a neural network without hidden layer and just mutate the values of the best bird
	 * @param distToPipe
	 * @param distToTopPipe
	 * @param distToBottomPipe
	 * @param velocityY
	 * @return
	 */
	public void jumps(double distToPipe, double distToTopPipe, double distToBottomPipe, double velocityY){
		double[] a1 = new double[]{distToPipe, distToTopPipe, distToBottomPipe, velocityY};
		
		double zJump = b1[0];
		for(int i = 0; i < w1_Jump.length; i++){
			zJump += w1_Jump[i] * a1[i];
		}
		
		double zNoJump = b1[1];
		for(int i = 0; i < w1_NoJump.length; i++){
			zNoJump += w1_NoJump[i] * a1[i];
		}
		
		double aJump = sigmoid(zJump);
		double aNoJump = sigmoid(zNoJump);
		
		
		if(aJump > aNoJump){
			this.jump();
		}
	}
	
	public String doubleArrayToString(double[] array){
		String output = "[";
		for(double d : array){
			output += d + ", ";
		}
		output.substring(0, output.length()-3);
		output += "]";
		return output;
	}
	
	public double sigmoid(double x){
		 return 1.0 / (1.0 + Math.exp(-x));
	}
	
	
	public String toString(){
		return  "w1_jump\n" + doubleArrayToString(w1_Jump) + "\n" + 
				"w1_noJump\n" + doubleArrayToString(w1_NoJump) + "\n" + 
				"b1\n" + doubleArrayToString(b1) +"\n\n\n\n\n\n";
	}
}
