import javafx.animation.AnimationTimer;

public class MyAnimationTimer extends AnimationTimer{

	private FlappyView view;
	
	public MyAnimationTimer(FlappyView view){
		this.view = view;
		
	}
		
	@Override
	public void handle(long currentNanoTime){
		if(view.isAI_GAME){
			handle_AI();
		}
		else{
			if(!view.getGame().ingame){
				this.stop();
				view.resetButton();
				return;
			}
			view.getGame().update();
			view.paintComponents();
		}
	}
	
	
	
	public void handle_AI(){
		
		
		
		if(!view.ai_game.ingame){
			this.stop();
			view.resetButton_AI();
			return;
		}
		
		view.ai_game.update();
		view.paintComponents();
	
		
	}
	

}
